<?PHP

namespace Elementor;
/**
 * Elementor image lists Widget.
 *
 *
 * @since 1.0.0
 */
class ELA_image_lists extends Widget_Base
{

    /**
     * Get widget name
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_name()
    {
        return 'Image lists';
    }

    /**
     * Get widget title.
     *
     *
     * @return string Widget title.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_title()
    {
        return __('Image Lists', 'ela-extension');
    }

    /**
     * Get widget icon.
     *
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_icon()
    {
        return 'dashicons dashicons-admin-page';
    }

    /**
     * Get widget categories
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_categories()
    {
        return ['basic'];
    }

    protected function _register_controls()
    {

        $this->register_content_control();
        $this->register_style_controls();

    }

    /**
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render()
    {
        $settings = $this->get_settings_for_display();

        ?>
        <div class="ela-gallery">
            <?PHP
            if (!empty($settings['item_list']) && sizeof($settings['item_list']) > 0):
                foreach ($settings['item_list'] as $image):
                    echo '<div class="ela-item image-' . $image['image-size'] . ' border-style-' . $settings['ela_border'] . ' el-col-' . esc_html($settings['ela-column']) . ' el-mobile-col-' . esc_html($settings['ela-column_mobile']) . ' el-tablet-col-' . esc_html($settings['ela-column_tablet']) . '">';
                    echo '<div class="image-item-back">';
                    echo '<img id="' . $image['ela-images']['_id'] . '" class="ela-img" src="' . esc_url_raw($image['ela-images']['url']) . '">';
                    echo '<div class="item_hover"><i class="' . $settings['icon'] . '" aria-hidden="true"></i></div>';
                    echo '</div>';
                    echo '</div>';
                endforeach;
            endif;
            ?>
        </div>
        <?PHP
    }

    protected function register_style_controls()
    {
        $this->start_controls_section(
            'style_section',
            [
                'label' => __('Image', 'ela-extension'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        //classic height image control
        $this->add_responsive_control(
            'ela-column',
            [
                'label' => __('Column', 'ela-extension'),
                'type' => Controls_Manager::SELECT,
                'default' => 4,
                'show_label' => true,
                'options' => [
                    1 => __('1', 'ela-extension'),
                    2 => __('2', 'ela-extension'),
                    3 => __('3', 'ela-extension'),
                    4 => __('4', 'ela-extension'),
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'ela_border',
                'label' => __('Border', 'ela-extension'),
                'selector' => '{{WRAPPER}} .ela-img',
            ]
        );

        $this->add_responsive_control(
            'ela_image_border_radius',
            [
                'label' => __('Border Radius', 'ela-extension'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ela-img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'ela-shadow',
                'label' => __('Box Shadow', 'ela-extension'),
                'selector' => '{{WRAPPER}} .ela-img',
            ]
        );

        $this->end_controls_section();

    }

    protected function register_content_control()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Content', 'ela-extension'),
                'tab' => Controls_Manager::TAB_CONTENT,
            ]
        );

        $repeater = new Repeater();

        $repeater->add_control(
            'ela-images',
            [
                'label' => __('Image', 'ela-extension'),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'image-size',
            [
                'label' => __('Images sizes', 'ela-extension'),
                'type' => Controls_Manager::SELECT,
                'default' => "original",
                'options' => [
                    'thumbnail' => __('Thumbnail 150*150', 'ela-extension'),
                    'original' => __('Original', 'ela-extension'),
                ],
            ]
        );

        $this->add_control(
            'item_list',
            [
                'label' => __('Repeater Image', 'ela-extension'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'list_title' => __('Image #1', 'ela-extension'),
                        'list_content' => __('Item content. Click the edit button to change this text.', 'plugin-domain'),
                    ],
                ],
            ]
        );

        $this->add_control(
            'icon',
            [
                'label' => __('Icon', 'ela-extension'),
                'type' => Controls_Manager::ICON,
                'default' => "fa fa-plus",
                "label_block" => true,
            ]
        );

        $this->end_controls_section();
    }


}