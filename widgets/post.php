<?PHP

namespace Elementor;

/**
 *
 *
 * @since 1.0.0
 */
class ELA_Posts extends Widget_Base
{

    protected $_has_template_content = false;

    /**
     * Get widget name.
     *
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_name()
    {
        return 'ela-posts';
    }

    /**
     * Get widget title.
     *
     *
     * @return string Widget title.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_title()
    {
        return __('post Lists', 'ela-extension');
    }

    /**
     * Get widget icon.
     *
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_icon()
    {
        return 'dashicons dashicons-format-aside';
    }

    /**
     * Get widget categories.
     *
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_categories()
    {
        return ['basic'];
    }

    public function render()
    {
    }

    public function render_plain_content()
    {
    }

    protected function _register_skins()
    {
        require_once(__DIR__ . '/skins/base-skin.php');
        require_once(__DIR__ . '/skins/skin-cards.php');
        require_once(__DIR__ . '/skins/skin-classic.php');
        $this->add_skin(new \ELA_Skin_Cards($this));
        $this->add_skin(new \ELA_Skin_Classic($this));
    }

    protected function _register_controls()
    {

        $this->register_content_control();

    }

    protected function register_content_control()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Content', 'ela-extension'),
                'tab' => Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->end_controls_section();
    }
}