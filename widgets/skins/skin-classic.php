<?PHP

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class ELA_Skin_Classic extends ELA_Skin_Base
{
    protected function _register_controls_actions()
    {
        parent::_register_controls_actions();
    }

    public function get_id()
    {
        return 'classic';
    }

    public function get_title()
    {
        return __('Classic', 'ela-extension');
    }

    /**
     * Render skin.
     *
     * Generates the final HTML on the frontend.
     *
     * @since 1.0.0
     * @access public
     */
    public function render()
    {
        $prefix = $this->get_id() . '_';
        $settings = $this->parent->get_settings();

        $args = [
            "post_type" => "post",
            "posts_per_page" => !empty($settings[$prefix . 'post_per_page']) ? $settings[$prefix . 'post_per_page'] : 4,
            'orderby' => 'date',
            'order' => 'DESC',
        ];
        $new_query = new \WP_Query($args);
        ?>
        <div class="ela-gallery">
        <?PHP
        if ($new_query->have_posts()) :
            while ($new_query->have_posts()):
                $new_query->the_post();
                $thumbnail_url = get_the_post_thumbnail_url();
                $post_column = esc_html($settings[$prefix . 'post_column']);
                $post_column_mobile = esc_html($settings[$prefix . 'post_column_mobile']);
                $post_column_tablet = esc_html($settings[$prefix . 'post_column_tablet']);
                $link = get_the_permalink();
                $title = get_the_title();
                $excerpt = get_the_excerpt();
                echo "<div class='ela-item ela-post-item el-col-$post_column el-mobile-col-$post_column_mobile el-tablet-col-$post_column_tablet '>";
                echo "<img src='$thumbnail_url'>";
                echo "<h2><a href='$link'>$title</a></h2>";
                echo "<span>$excerpt</span>";
                echo "</div>";
            endwhile;
            echo "</div>";
        endif;
    }
}
