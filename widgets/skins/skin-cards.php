<?PHP

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class ELA_Skin_Cards extends ELA_Skin_Base
{

    protected function _register_controls_actions()
    {
        parent::_register_controls_actions();

        add_action('elementor/element/ela-posts/content_section/before_section_end', [$this, 'cards_register_additional_design_controls']);
    }

    /**
     * @return string
     */
    public function get_id()
    {
        return 'card';
    }

    /**
     * @return string
     */
    public function get_title()
    {
        return __('Card', 'ela-extension');
    }

    /**
     * @param Widget_Base $widget
     */
    public function cards_register_additional_design_controls(Widget_Base $widget)
    {
        $this->parent = $widget;
        $this->add_responsive_control(
            'show_image',
            [
                'label' => __('Show Image', 'ela-extension'),
                'type' => Controls_Manager::SELECT,
                'show_label' => true,
                'default' => true,
                'options' => [
                    true => __('Yes', 'ela-extension'),
                    false => __('No', 'ela-extension'),
                ],
            ]
        );

    }

    /**
     * Render skin.
     *
     * Generates the final HTML on the frontend.
     *
     * @since 1.0.0
     * @access public
     */
    public function render()
    {
        $prefix = $this->get_id() . '_';
        $settings = $this->parent->get_settings();
        $args = [
            "post_type" => "post",
            "posts_per_page" => !empty($settings[$prefix . 'post_per_page']) ? $settings[$prefix . 'post_per_page'] : 4,
            'orderby' => 'date',
            'order' => 'DESC',
        ];
        $new_query = new \WP_Query($args);
        ?>
        <div class="ela-gallery">
        <?PHP
        if ($new_query->have_posts()) :
            while ($new_query->have_posts()):
                $new_query->the_post();
                ?>
                <div class="ela-item card-skin ela-post-item el-col-<?PHP echo $settings[$prefix . 'post_column'] ?> el-mobile-col-<?PHP echo($settings[$prefix . 'post_column_mobile']) ?> el-tablet-col-<?PHP echo $settings[$prefix . 'post_column_tablet'] ?> ">
                    <div class="card-skin-back">
                        <div class="card-skin-top">
                            <?PHP if ($settings[$prefix . 'show_image']) { ?>
                                <img src="<?PHP echo get_the_post_thumbnail_url(); ?>">
                                <div class="card-skin-top-cat">
                                    <?PHP foreach (get_the_category() as $item) { ?>
                                        <a href="<?PHP echo get_category_link($item->term_id) ?>"><?PHP echo $item->name ?></a>
                                        <?PHP
                                    } ?>
                                </div>
                            <?PHP } ?>
                            <img class="ela-post-item-avatar"
                                 src="<?PHP echo get_avatar_url(get_the_author_meta("ID")) ?>"
                                 alt="avatar">
                        </div>
                        <div class="card-skin-bottom">
                            <h2><a href="<?PHP echo get_the_permalink() ?>"><?PHP echo get_the_title() ?></a></h2>
                            <span><?PHP echo get_the_excerpt() ?></span>
                            <div><a href="<?PHP echo get_the_permalink() ?>" class="ela-post-item-read-more"><?PHP _e("Read More =>","ela-extention")?></a>
                            </div>
                        </div>

                    </div>
                </div>

            <?PHP
            endwhile;
            ?>
            </div>
        <?PHP
        endif;
    }
}