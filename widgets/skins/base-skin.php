<?PHP


use Elementor\Controls_Manager;
use Elementor\Skin_Base as Elementor_Skin_Base;
use Elementor\Widget_Base;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

abstract class ELA_Skin_Base extends Elementor_Skin_Base
{

    protected function _register_controls_actions()
    {
        add_action('elementor/element/ela-posts/content_section/before_section_end', [$this, 'register_additional_design_controls']);
    }

    public function register_additional_design_controls(Widget_Base $widget)
    {

        $this->parent = $widget;

        $this->add_responsive_control(
            'post_column',
            [
                'label' => __('Columns', 'ela-extension'),
                'type' => Controls_Manager::SELECT,
                'show_label' => true,
                'default' => 4,
                'options' => [
                    1 => __('1', 'ela-extension'),
                    2 => __('2', 'ela-extension'),
                    3 => __('3', 'ela-extension'),
                    4 => __('4', 'ela-extension'),
                ],
            ]
        );

        $this->add_control(
            'post_per_page',
            [
                'label' => __('Posts per page', 'ela-extension'),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );
    }

}